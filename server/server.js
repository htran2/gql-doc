const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const axios = require('axios');
const fs = require('fs');

const port = 3001;

const NODE_ENV = process.env.NODE_ENV || 'local';
if (NODE_ENV === 'local') {
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
}

const app = express();
app.use(cors(), bodyParser.json());

const query = fs.readFileSync('./introspection-query.graphql', 'utf8').toString();
const examples = fs.readFileSync('./examples.json', 'utf8').toString();

const ssoTokenSandbox = 'AjExMDAgAA1wb3J0YWw6RTg3OTkyiAATYmFzaWNhdXRoZW50aWNhdGlvbgEABkU4Nzk5MgIAAzk5OQMAA1NQMgQADDIwMjEwOTEzMTcxMgUABAAAAAwKAAZFODc5OTL%2FARcwggETBgkqhkiG9w0BBwKgggEEMIIBAAIBATELMAkGBSsOAwIaBQAwCwYJKoZIhvcNAQcBMYHgMIHdAgEBMDIwKjELMAkGA1UEBhMCQ0ExDTALBgNVBAsTBEoyRUUxDDAKBgNVBAMTA1NQMgIEN21GajAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMjEwOTEzMTcxMjE4WjAjBgkqhkiG9w0BCQQxFgQUG90UcxrWryFta2QbD!TmcDitiF0wCQYHKoZIzjgEAwQvMC0CFEaQHbnZc%2Fy1Gqu92D27sxgDgp8HAhUAkZIfwPhwuL0L0s81gnGf0U4N%2FZg%3D';

const ssoTokenQ1 = 'AjExMDAgABBwb3J0YWw6VFNfR1FMXzAxiAATYmFzaWNhdXRoZW50aWNhdGlvbgEACVRTX0dRTF8wMQIAAzk5OQMAA1FQMQQADDIwMjExMDAxMTUxNAUABAAAAAwKAAlUU19HUUxfMDH%2FASEwggEdBgkqhkiG9w0BBwKgggEOMIIBCgIBATELMAkGBSsOAwIaBQAwCwYJKoZIhvcNAQcBMYHqMIHnAgEBMDwwMzELMAkGA1UEBhMCQ0ExFjAUBgNVBAoTDUFUQiBGaW5hbmNpYWwxDDAKBgNVBAMTA1FQMQIFAIy8jsgwCQYFKw4DAhoFAKBdMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTIxMTAwMTE1MTQzOVowIwYJKoZIhvcNAQkEMRYEFMpsFGv7qrJrDIIHl0Kc4CrbEl0LMAkGByqGSM44BAMELzAtAhQ1d8khF6OB6N8U%2FfEETH64dwMWzgIVAJrpCfc3CE0VchSpc2ufaU0bAWfo';

const mine = {
  method: 'POST',
  url: 'http://localhost:7000/graphql',
  headers: {'Content-Type': 'application/json'},
  data: query
};
const sandbox = {
  method: 'POST',
  url: 'http://coresapsbx002.atb.ab.com:8002/sap/bc/atb/graphql/businesstrans?sap-client=510',
  params: {'sap-client': '510'},
  headers: {
    'Content-Type': 'application/json',
    MYSAPSSO2: ssoTokenSandbox
  },
  data: JSON.stringify({query: query})
};

const introspect = (req) => {
  const { sapGqlUrl, sapGqlClient } = req.query;
  const requestOptions = {
    method: 'POST',
    url: `https://crm.sit.insideatb.net/sap/bc/atb/graphql/${sapGqlUrl}`,
    params: {'sap-client': sapGqlClient},
    headers: {
      'Content-Type': 'application/json',
      MYSAPSSO2: ssoTokenQ1
    },
    data: JSON.stringify({query: query})
  };
  return axios.request(requestOptions);
}

app.get('/', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.end(`<h1>Hello from graphql documentation!</h1>
    <h2>Schemas from SAP graphql services:</h2>
    <a href='schema?sapGqlUrl=businesspartner&sapGqlClient=100'>Business Partner</a><br/>
    <a href='schema?sapGqlUrl=cop&sapGqlClient=100'>Customer Obsession Plan (COP)</a><br/>
  `);
});

app.get('/examples', (req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/html');
  res.write(examples);
  res.end();
});

app.get('/schema', (req, res) => {
  introspect(req).then(response => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(response.data.data));
    res.end();
  }).catch(function (error) {
    console.error(error);
    res.setHeader('Content-Type', 'text/html');
    res.end(error);
  });
});


app.listen(port, () => console.info(`Server started on port ${port}`));

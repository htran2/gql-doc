import { useState, useEffect } from 'react';
import { Tabs, Collapse, Spin } from 'antd';
import gqllogo from './GraphQL_Logo.svg';
import Examples from './Examples';
import RawSchema from './RawSchema';
import { loadSchemaStatic } from './services';

const isBuiltInType = typeName => typeName == null || typeName.startsWith('__') || ['QueryType', 'MutationType', 'ID', 'String', 'Boolean', 'Int', 'Float'].includes(typeName);

const Schema = ({ sapGqlUrl, sapGqlClient }) => {
  const [raw, setRaw] = useState();
  const [types, setTypes] = useState();
  useEffect(() => {
    loadSchemaStatic({ sapGqlUrl, sapGqlClient }).then(response => {
      setTypes(response.default.__schema.types);
      setRaw(response.default);
    }).catch(function (error) {
      console.error(error);
    });
  }, [sapGqlUrl, sapGqlClient]);
  if (!types) {
    return <h2>Loading... <Spin size="large" /></h2>
  }
  const items = types.map((t) => {
    if (isBuiltInType(t.name)) {
      return null;
    }
    if (t.name.endsWith('Input') || t.kind === 'INPUT_OBJECT') {
      return <Input {...t} />;
    } else if (t.name.endsWith('Output')) {
      return <Output {...t} />;
    } else if (t.name.endsWith('EnumType') || t.name.endsWith('Enum') || t.kind === 'ENUM') {
      return <Enum {...t} />;
    } else if (t.kind === 'OBJECT' && (t.fields && t.fields.length)) {
      return <Objeckt {...t} />
    }
    return (
      <div id={t.name} className="type">
        <span className="undocumented">{t.name}</span>
        <span className="undocumented">{` docu ${t.kind}`}</span>
      </div>
    );
  });
  let queries, mutations;
  const queryType = types.filter(t => t.name === 'QueryType');
  if (queryType && queryType.length === 1) {
    queries = (queryType[0]).fields.map(q => <Query {...q} />);
  }
  const mutationType = types.filter(t => t.name === 'MutationType');
  if (mutationType && mutationType.length === 1) {
    mutations = (mutationType[0]).fields.map(m => <Mutation {...m} />);
  }
  
  return (
    <div className="schema">
      <Tabs defaultActiveKey="doc">
        <Tabs.TabPane tab="Doc" key="doc">
          <div className="section">{queries}</div>
          <div className="section">{mutations}</div>
          <div className="section">{items}</div>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Raw" key="raw">
          <RawSchema data={raw} />
        </Tabs.TabPane>
      </Tabs>
      <img src={gqllogo} height="50" className="graphql-logo" alt="graphql" />
    </div>
  );
}

const Query = (q) => <RootType epType="query" {...q} />;
const Mutation = (m) => <RootType epType="mutation"  {...m} />;
const RootType = (t) => { // gql entry-point type (i.e. not object or enum type)
  // special case, no display
  if ('DUMMY' === t.name) {
    return null;
  }
  let args = <div className="value">No input</div>;
  if (t.args) {
    args = t.args.map(a => <Field {...a} />);
  }
  let output = <div className="value">No output</div>;
  if (t.type.name && t.type.kind === 'OBJECT') {
    // output as one object
    output = <a href={`#${t.type.name}`} className="value">{t.type.name}</a>;
  } else if (t.type.kind === 'LIST' && t.type.ofType) {
    // output as list
    output = <a href={`#${t.type.ofType.name}`} className="value">{`${t.type.ofType.name}[]`}</a>;
  }
  return (
    <div className="type">
      <Heading type={t.epType} name={t.name} description={t.description} />
      <div className="content">
        <Collapse bordered={false} defaultActiveKey={['in', 'out']}>
          <Collapse.Panel header="Inputs" key="in">
            {args}
          </Collapse.Panel>
          <Collapse.Panel header="Output" key="out">
            {output}
          </Collapse.Panel>
          <Collapse.Panel header="Examples" key="ex">
            <Examples data="TODO: SHOW EXAMPLES" />
          </Collapse.Panel>
        </Collapse>
      </div>
    </div>
  );
}

const Input = (i) => {
  const fields = i.inputFields.map(f => <Field {...f} />);
  return (
    <div id={i.name} className="type">
      <Heading type="input" name={i.name} description={i.description} />
      <div className="content">
        {fields}
      </div>
    </div>
  );
}

const Output = (o) => {
  const fields = o.fields.map(f => <Field {...f} />);
  return (
    <div id={o.name} className="type">
      <Heading type="output" name={o.name} description={o.description} />
      <div className="content">
        {fields}
      </div>
    </div>
  );
}

const Enum = (e) => {
  const values = e.enumValues.map(v => 
    <div className="enum-item">
      <span className="enum-value">{v.name}</span>
      {v.description ? <div className="description">{v.description}</div> : null}
    </div>
  );
  return (
    <div id={e.name} className="type">
      <Heading type="enum" name={e.name} description={e.description} />
      <div className="content">
        {values}
      </div>
    </div>
  );
}

const Objeckt = (o) => {
  const fields = o.fields.map(f => <Field {...f} />);
  return (
    <div id={o.name} className="type">
      <Heading type="object" name={o.name} description={o.description} />
      <div className="content">
        {fields}
      </div>
    </div>
  );  
}

const Heading = ({type, name, description}) => {
  return (
    <div className="heading">
      <div className={`heading-title ${type}`}>{name}</div>
      <Tag type={type} />
      {description ? <div className="description">{description}</div> : null}
    </div>
  );
}

const Field = ({type, name, description, defaultValue}) => {
  const typeName = (type.ofType && type.ofType.name) || '__';
  return (
    <div className="value" key={name}>
      {`${name}: `}
      {type.ofType ? (isBuiltInType(typeName) ? typeName : (<a href={`#${typeName}`}>{typeName}</a>)) : null}
      {type.kind === 'LIST' ? '[]' : null}
      {type.kind === 'NON_NULL' ? <span className="mandatory-arg">!</span> : null}
      {(isBuiltInType(type.name) ? type.name : (<a href={`#${type.name}`}>{type.name}</a>))}
      {defaultValue ? ` = ${defaultValue}` : null}
      {description ? <span className="description inline">{description}</span> : null}
    </div>
  );
}

const Tag = ({type}) => {
  return (
    <div className={`tag ${type}`}>
      {type}
    </div>
  );
}

export default Schema;

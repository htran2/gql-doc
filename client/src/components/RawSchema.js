import { Button } from 'antd';
import { CopyOutlined } from '@ant-design/icons';

const RawSchema = ({data}) => {
  return (
    <div className="raw">
      <Button clasName="copy-button" icon={<CopyOutlined />} type="text"/>   
      <p><code>{JSON.stringify(data)}</code></p>
    </div>
  );
}

export default RawSchema;

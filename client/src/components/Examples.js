const Examples = ({data}) => {
  return (
    <div className="examples">
      {data}
    </div>
  );
}

export default Examples;

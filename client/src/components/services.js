import axios from "axios";

const server = 'http://localhost:3001';
const schemasUrl = `${server}/schema`;
const examplesUrl = `${server}/examples`;

const loadSchema = ({ sapGqlUrl, sapGqlClient }) => {
  return axios.request(schemasUrl, { params: { sapGqlUrl, sapGqlClient }});
};
const loadSchemaStatic = ({ sapGqlUrl, sapGqlClient }) => {
  return import(`./static-raw/${sapGqlUrl}.json`);
};
const loadExamples = () => {
  return axios.request(examplesUrl); // TODO work on me
};

export {
  loadSchemaStatic,
  loadExamples
}
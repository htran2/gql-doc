import './App.scss';
import { Collapse, Card, Space } from 'antd';
import Schema from './components/Schema';

const App = () => {
  const schemas = [
    { name: 'Business Partners', description: 'Clients, branches, contacts, relationships, marketings,...', sapGqlUrl: 'businesspartner', sapGqlClient: '100' },
    { name: 'Business Transactions', description: 'Activities, tasks, leads, opportunities,...', sapGqlUrl: 'businesstrans', sapGqlClient: '100' },
    { name: 'COPs', description: 'Customer Obsession Plans', sapGqlUrl: 'cop', sapGqlClient: '100' },
    { name: 'Value Helps', description: 'Common values, enums,...', sapGqlUrl: 'valuehelp', sapGqlClient: '100' },
    { name: 'AO Value Helps', description: 'Products, categories, facilities, transits,...', sapGqlUrl: 'aovaluehelp', sapGqlClient: '100' },
    { name: 'MasterCard', description: '', sapGqlUrl: 'mastercard', sapGqlClient: '100' },
  ];
  const allCRMPanels = schemas.map(({name, description, sapGqlUrl, sapGqlClient}) => {
    const title = <span className="schema-title">{name}</span>;
    const desc = <span className="schema-description">{description}</span>;
    return (
      <Collapse.Panel header={<span>{title}{desc}</span>} key={sapGqlUrl} forceRender>
        <Schema sapGqlUrl={sapGqlUrl} sapGqlClient={sapGqlClient} />
      </Collapse.Panel>
    );
  });
  return (
    <div className="App">
      <Space direction="vertical" size="large">
        <Card title={<h1>SAP CRM</h1>}>
          <Collapse>{allCRMPanels}</Collapse>
        </Card>
        <Card title={<h1>SAP BaS</h1>}>
          <p>TODO: FILL ME</p>
        </Card>
      </Space>
    </div>
  );
}

export default App;

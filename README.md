# gql-doc

## About
Document ATB's SAP graphql services.

This is a webapp. Its code contains two modules:
- server (Node express), to read graphql schemas and feed the front end
- front end (React), to showcase schemas and some conveniences: global search, cross references, "try-me", etc.


## Developer's set up
Clone, then `cd gql-doc/server`
`npm install` (one time)
`npm start`
Visit `localhost:3001`. You should see a welcome message and some links. Click them!

In another terminal, `cd gql-doc/client`
`npm install` (one time)
`npm start`

Your default browser should open a page with schema contents.

## TODOs

Server:
Stop using `process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";`
Read from gitlab `graphQL_insomnia_endpoint_export.json` to get examples

Front-end:
Use AntD
Beautify JSON and examples
Global search
Graphs
Playground
Bookmarkable
Environments